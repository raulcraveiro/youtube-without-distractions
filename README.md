[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad?style=flat-square)](https://raw.githubusercontent.com/raulcraveiro/youtube-without-distractions/production/youtube-without-distractions.user.css) [![Twitter Follow](https://img.shields.io/twitter/follow/raulcraveiro?color=00acee&logo=twitter&style=flat-square)](https://twitter.com/raulcraveiro) [![GitHub](https://img.shields.io/github/license/raulcraveiro/youtube-without-distractions?style=flat-square)](https://github.com/raulcraveiro/youtube-without-distractions/blob/master/LICENSE)

# Youtube Without Distractions

O Youtube é um perigo para a procrastinação, com a quantidade de vídeos diferentes a todo momento na página inicial. Com uma pequena customização através de CSS, é possível remover isto e ajudar a focar no que realmente interessa.

## Instalação

Basta instalar a extensão [Stylus](https://github.com/openstyles/stylus) e depois [instalar este userstyle](https://raw.githubusercontent.com/raulcraveiro/youtube-without-distractions/master/youtube-without-distractions.user.css). Simples assim.
